# uses the multi-stage build pattern to  1) create the binary and  2) package the binary into an image
# use maven as the builder base image
FROM maven:3.6.3-openjdk-11 AS builder

# copy pom into the builder container
COPY pom.xml pom.xml

# copy source into the container
COPY src/main src/main

# build and test the binary
RUN mvn clean verify


# use an openjdk 11 image to package the binary
FROM adoptopenjdk/openjdk11:alpine-jre

# create the directory to store the dispatcher resources
RUN mkdir -p /dms-dispatcher

# set the app directory as the current working directory
WORKDIR /dms-dispatcher

# restrict the application permissions by creating and then running as a standard user
RUN addgroup -S dms-dispatcher && adduser -S dms-dispatcher -G dms-dispatcher

# copy only the final fat jar from the builder container
COPY --from=builder target/dispatcher-*.jar app.jar

# make the dispatcher user the owner of the app directory
RUN chown -R dms-dispatcher:dms-dispatcher /dms-dispatcher

# change to the dispatcher user
USER dms-dispatcher:dms-dispatcher

# run the application as the default entrypoint
ENTRYPOINT ["java","-jar","app.jar"]