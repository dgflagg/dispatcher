#!/bin/bash

while getopts "u:p:" opt
do
   case "$opt" in
      u ) SPRING_MAIL_USERNAME="$OPTARG" ;;
      p ) SPRING_MAIL_PASSWORD="$OPTARG" ;;
   esac
done

# pull image locally
docker pull us.gcr.io/dgflagg-dms-api/dms-dispatcher:latest

# stop the currently running dispatcher container
docker rm -f dms-dispatcher

# run the dispatcher as a docker container
docker run --rm -d --name dms-dispatcher \
    -e GOOGLE_APPLICATION_CREDENTIALS=/dms-dispatcher/secrets/dgflagg-dms-dispatcher.json \
    -e "spring.mail.username=${SPRING_MAIL_USERNAME}" \
    -e "spring.mail.password=${SPRING_MAIL_PASSWORD}" \
    -v "$(pwd):/dms-dispatcher/secrets" us.gcr.io/dgflagg-dms-api/dms-dispatcher:latest