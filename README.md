Reads messages from topic and 'dispatches' the messages by email

# build docker image locally (no build cache)
docker build -t dms-dispatcher:local .

# run container locally
docker run --rm -d --name dms-dispatcher \
    -e GOOGLE_APPLICATION_CREDENTIALS=/dms-dispatcher/secrets/dgflagg-dms-dispatcher.json \
    -e "spring.mail.username=${SPRING_MAIL_USERNAME}" \
    -e "spring.mail.password=${SPRING_MAIL_PASSWORD}" \
    -v "$(pwd):/dms-dispatcher/secrets" dms-dispatcher:local

//TODO:
- unit tests