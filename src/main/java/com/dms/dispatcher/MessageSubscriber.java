package com.dms.dispatcher;

public interface MessageSubscriber {
    void awaitMessages();
}
