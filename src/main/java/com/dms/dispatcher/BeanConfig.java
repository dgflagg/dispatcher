package com.dms.dispatcher;

import org.springframework.context.annotation.Bean;


public class BeanConfig {

    @Bean
    public MessageDispatcherService dispatcherService() {
        return new EmailMessageDispatcherService();
    }

}
