package com.dms.dispatcher;

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class GoogleCloudPubSubMessageSubscriber implements MessageSubscriber {

    private MessageDispatcherService dispatcherService;
    private ProjectSubscriptionName subscriptionName;


    public GoogleCloudPubSubMessageSubscriber(  @Autowired MessageDispatcherService dispatcherService,
                                                @Value("${gc-project-id}") String gcProjectId,
                                                @Value("${gc-pubsub-subscription-name}") String gcPubsubSubscriptionName) {
        this.dispatcherService = dispatcherService;
        this.subscriptionName = ProjectSubscriptionName.of(gcProjectId, gcPubsubSubscriptionName);

        this.awaitMessages();
    }

    @Override
    public void awaitMessages() {
        // Instantiate an asynchronous message receiver.
        MessageReceiver receiver =
                (PubsubMessage message, AckReplyConsumer consumer) -> {
                    // Handle incoming message, then ack the received message.
                    log.info("Id: '{}'", message.getMessageId());
                    log.info("Data: '{}'", message.getData().toStringUtf8());

                    ScheduledMessage scheduledMessage = ScheduledMessage.fromJson(message.getData().toStringUtf8());
                    dispatcherService.dispatch(scheduledMessage);

                    consumer.ack();
                };

        Subscriber subscriber = Subscriber.newBuilder(subscriptionName, receiver).build();
        // Start the subscriber.
        subscriber.startAsync().awaitRunning();

        log.info("Listening for messages on: '{}'", subscriptionName.toString());
        // Allow the subscriber to until an unrecoverable error occurs.
        subscriber.awaitTerminated();

        // Shut down the subscriber to stop receiving messages.
        subscriber.stopAsync();
    }

}
