package com.dms.dispatcher;

import com.jayway.jsonpath.JsonPath;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduledMessage {
    private String id;
    private String email;
    private String content;

    @Override
    public String toString() {
        return "id: " + id + ", email: " + email + ", content: " + content;
    }

    /**
     * Parses a json representation of a message into a ScheduledMessage object and returns the ScheduledMessage object
     * @param json
     * @return
     */
    public static ScheduledMessage fromJson(String json) {
        ScheduledMessage scheduledMessage = new ScheduledMessage();
        scheduledMessage.setId(JsonPath.read(json, "$.id"));
        scheduledMessage.setEmail(JsonPath.read(json, "$.email"));
        scheduledMessage.setContent(JsonPath.read(json, "$.content"));
        return scheduledMessage;
    }
}
