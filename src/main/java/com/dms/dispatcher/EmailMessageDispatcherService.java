package com.dms.dispatcher;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Slf4j
@Service
public class EmailMessageDispatcherService implements MessageDispatcherService {

    @Autowired
    private JavaMailSender javaMailSender;


    public EmailMessageDispatcherService() {

    }

    @Override
    public void dispatch(ScheduledMessage scheduledMessage) {
        MimeMessage msg = javaMailSender.createMimeMessage();

        log.info(scheduledMessage.toString());

        // true = multipart message
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(msg, true);
            helper.setTo(scheduledMessage.getEmail());
            helper.setSubject("Message from DMS");

            // default = text/plain
            helper.setText(scheduledMessage.getContent());

            // true = text/html
            //helper.setText("<h1>Check attachment for image!</h1>", true);

            // hard coded a file path
            //FileSystemResource file = new FileSystemResource(new File("path/android.png"));
            //helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));
        } catch (MessagingException e) {
            log.error("Failed to send message: {}", e.getMessage());
        }

        javaMailSender.send(msg);
    }

}
