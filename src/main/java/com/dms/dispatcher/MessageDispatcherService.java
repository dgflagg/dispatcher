package com.dms.dispatcher;

public interface MessageDispatcherService {
    void dispatch(ScheduledMessage scheduledMessage);
}
